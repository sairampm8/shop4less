import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Product } from './product'
import { Observable } from 'rxjs';
import { Category } from '../site-framework/category'

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  

  constructor(private http:HttpClient) { }

  getAllProducts(): Observable<Product[]>{
    const productUrl = "http://localhost:3000/products";
    return this.http.get<Product[]>(productUrl) 
  }

  getCategories(): Observable<Category[]>{
    const categoriesUrl = "http://localhost:3000/categories";
    return this.http.get<Category[]>(categoriesUrl)
  }

  createProduct(productBody): Observable<Product[]>{
    const productUrl = "http://localhost:3000/products";
    return  this.http.post<Product[]>(productUrl, productBody)
  }
  

  viewProduct(productId){
    const  productUrl = "http://localhost:3000/products/"+productId; 
    return this.http.get(productUrl, productId)
  }

  updateProduct(productId,productBody){
    const  productUrl = "http://localhost:3000/products/"+productId; 
    return this.http.put(productUrl, productBody)
  }

  deleteProduct(productId){
    const  productUrl = "http://localhost:3000/products/"+productId; 
    return this.http.delete(productUrl, productId)
  }

  searchCategoryProducts(categoryId){
    const  productUrl = "http://localhost:3000/products?categoryId="+categoryId; 
    return this.http.get(productUrl, categoryId)
  }

  searchDateProducts(dateParam): Observable<Product[]>{
    const  productUrl = "http://localhost:3000/products/date="+dateParam; 
    return this.http.get<Product[]>(productUrl)
  }
}
