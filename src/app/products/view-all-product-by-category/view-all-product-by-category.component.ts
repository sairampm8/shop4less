import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/site-framework/category';
import { Product } from '../product';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-view-all-product-by-category',
  templateUrl: './view-all-product-by-category.component.html',
  styleUrls: ['./view-all-product-by-category.component.css']
})
export class ViewAllProductByCategoryComponent implements OnInit {

  searchCategory : Category
  productList : Product[]

  constructor(private activatedroute: ActivatedRoute,
              private productService: ProductsService) { }

  ngOnInit(): void {
    this.activatedroute.params.subscribe(data=>{
      console.log(data)
      this.searchCategory = data.id
      this.productService.searchCategoryProducts(this.searchCategory ).subscribe(categoryData=>{
        this.productList = categoryData
        console.log(categoryData)
      })
    })
  }

}
